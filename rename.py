#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Rename forward and reverse reads from Genoscope raw sequencing file

import sys
import os
import os.path
import subprocess
import argparse
import datetime
import logging
import re
import csv

def runcmd(cmd):
    p=subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True, env=os.environ)
    logger.debug(cmd)
    p.wait()
    (out, err)=p.communicate()
    if isinstance(out, bytes):
        out=out.decode("utf-8")
    if p.returncode != 0 and err is not None:
        logger.debug("p = {} -- OUT = {} -- ERR = {}".format(str(p.returncode), out, err))
        logger.debug("ERROR : Process aborted")
        sys.exit(1)
    else:
        return out

def handleArgs():
    # Arguments definition
    parser = argparse.ArgumentParser("Rename fastq files from Original name to Abyss project name : File format handled : CCU_AANOOSTA_1_1_H7337BCX2.12BA079_clean.fastq.gz and _R[1-2].fastq.gz")
    parser.add_argument("--input-dir", dest="input",
                            help="Absolute path to input directory", metavar="PATH", required=True)
    parser.add_argument("--output-dir", dest="output",
                            help="Path to output directory", metavar="PATH", required=True)
    parser.add_argument("--method", dest="method",
                            help="copy or link renamed files", metavar="cp (default) OR ln", default="cp")
    parser.add_argument("--abyss-sample-name", dest="abysssamplename",
                            help="Path to csv file with original and abyss sample name", metavar="FILE", required=True)
    # Arguments initialization
    args = parser.parse_args()
    input = args.input
    if not input.startswith("/"):
        logger.debug("Input path is not absolute (must begin by '/') : {}".format(input))
        sys.exit(1)
    output = args.output
    method = args.method
    abysssamplename= args.abysssamplename
    if not os.path.isdir(input):
            parser.error("Input directory {} does not exist".format(input))
            sys.exit(1)
    if not os.path.isdir(output):
            os.makedirs(output) 
    if not method in [ "cp" , "ln"]:
            parser.error("Invalid method {}. You should choose between cp (copy) or ln (link)".format(method))
            sys.exit(1)
    if method == "ln":
            method = "ln -s"
    if not os.path.isfile(abysssamplename):
            parser.error("Corresponding file between Original and Abyss sample name doesnt not exit : {}".format(abysssamplename))    
            sys.exit(1)
    return input, output, method, abysssamplename


if __name__ == '__main__':

    # logging config
    logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)-5.5s]  %(message)s")

    logger = logging.getLogger()

    # Handle script arguments
    input, output, method, abysssamplename = handleArgs()

    #select input files
    readsfiles=os.listdir(input)
    #forward read patterngenoscope
    # CCR_AAEXOSTA_2_1_HGWCMBCX2.12BA308_clean.fastq.gz
    patterngenoscope = re.compile("(.)+_[1-9]_1_(.)+fastq.gz")
    # standard R1/R2 pattern
    patternclassic = re.compile("(.)+_R1.fastq.gz")
    for rf in readsfiles :
        R1=""
        R2=""
        filename=""
        # get a forward read
        if patterngenoscope.match(rf) :
            R1=rf
            # get the reverse read name
            R2=re.sub(r'(_[0-9])_1_', r'\1_2_', R1)
            # get the genoscope base name as it is written in CSV corresponding file
            filename=re.sub(r'(_[0-9])_1_', r'\1_',R1.replace("_clean","").replace(".fastq.gz",""))
        else :
            if patternclassic.match(rf):
                R1 = rf
                R2 = R1.replace("R1.fastq","R2.fastq")
                filename=R1.replace("_R1.fastq.gz","")
            else :
                logger.debug("Skip {} because it does not match R1 patterns (.)+_[1-9]_1_(.)+fastq.gz or (.)+_R1.fastq.gz".format(rf))
        if R1 and R2 and filename:
            abyssname=""
            with open(abysssamplename) as csvfile:
                csvreader=csv.reader(csvfile, delimiter=';')
                for row in csvreader:
                     #if genoscope file name in first column match the treated sample
                     #return the abyss name
                     if os.path.basename(filename) in row[0]:
                         abyssname=row[1]            
                         break
            cmdR1="{} {}/{} {}/{}_R1.fastq.gz".format(method,input,R1,output,abyssname)
            runcmd(cmdR1)
            cmdR2="{} {}/{} {}/{}_R2.fastq.gz".format(method,input,R2,output,abyssname)
            runcmd(cmdR2)
