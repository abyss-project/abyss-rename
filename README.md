## abyss-rename 
This script can be used to rename samples from ABYSS project.

## What it does
From original name example = CCU_ABENOSTA_2_2_H3WJNBCX2.12BA139_clean.fastq.gz get corresponding Abyss name = ESSNAUT_PL06_CT4_0_1_pool_DNA_10g_SLC_rep1_R2.fastq.gz
From standard _R1.fastq.gz / _R2.fastq.gz files, get corresponding Abyss name

## Inputs and Outputs
* Input files must have this format : XXXXXXXXX_[Lane_Number]_[Read_number]_FLOWCELLname.SAMPLEname_clean.fastq.gz or XXX_R[1-2].fastq.gz 
* Output files will be suffixed with _R1.fastq.gz or _R2.fastq.gz

## Parameters 
Parameters :
* input-dir : input directory with reads to rename (must be absolute path)
* output-dir : directory that will contain the renamed reads with their Abyss corresponding name
* method : copy original files (cp) [default]  or just link (symbolic link) (ln)
* abyss-sample-name : CSV file with original and Abyss corresponding names

## Corresponding CSV file
abyss-sample-name CSV file Example : 
```
Code_GENOSCOPE;Code_ABYSS;Primers
CCU_AANFOSTA_1_H7337BCX2.12BA162;ESSNAUT_PL06_CT2_0_1_DNA_10g_NEB_NUII_rep1;16S V4V5 Archae 517F/958R
CCU_AANGOSTA_5_H7337BCX2.12BA174;ESSNAUT_PL06_CT2_0_1_DNA_10g_NEB_NUII_rep2;16S V4V5 Archae 517F/958R
sample1;echA;16S V4V5 Archae 517F/958R
```

## How to use
Command-line example :
```
python rename.py --input-dir $PWD/test-data --output-dir $PWD/res --method ln --abyss-sample-name $PWD/test-data/names.csv
```

## Get help 
```
> python rename.py -h
usage: Rename fastq files from Original name to Abyss project name : File format handled : CCU_AANOOSTA_1_1_H7337BCX2.12BA079_clean.fastq.gz and _R[1-2].fastq.gz
       [-h] --input-dir PATH --output-dir PATH [--method cp default OR ln]
       --abyss-sample-name FILE

optional arguments:
  -h, --help            show this help message and exit
  --input-dir PATH      Absolute path to input directory
  --output-dir PATH     Path to output directory
  --method cp (default) OR ln
                        copy or link renamed files
  --abyss-sample-name FILE
                        Path to csv file with original and abyss sample name

```
